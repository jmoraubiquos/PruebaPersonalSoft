package co.com.personalsoft.model;

import java.io.Serializable;

/**
 *
 * @author jmora
 */
public class Producto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer prdCodigo;

    private String prdNombre;

    private String prdDescripcion;

    private String prdEstado;

    private GrupoProducto grupoProducto;

    public Producto() {
        this.grupoProducto=new GrupoProducto();
    }

    public Producto(Integer prdCodigo) {
        this.prdCodigo = prdCodigo;
    }

    public Producto(Integer prdCodigo, String prdNombre) {
        this.prdCodigo = prdCodigo;
        this.prdNombre = prdNombre;
    }

    public Producto(Integer prdCodigo, String prdNombre, String prdDescripcion, String prdEstado) {
        this.prdCodigo = prdCodigo;
        this.prdNombre = prdNombre;
        this.prdDescripcion = prdDescripcion;
        this.prdEstado = prdEstado;
    }

    public Producto(Integer prdCodigo, String prdNombre, String prdDescripcion, String prdEstado, GrupoProducto grupoProducto) {
        this.prdCodigo = prdCodigo;
        this.prdNombre = prdNombre;
        this.prdDescripcion = prdDescripcion;
        this.prdEstado = prdEstado;
        this.grupoProducto = grupoProducto;
    }

    public Integer getPrdCodigo() {
        return prdCodigo;
    }

    public void setPrdCodigo(Integer prdCodigo) {
        this.prdCodigo = prdCodigo;
    }

    public String getPrdNombre() {
        return prdNombre;
    }

    public void setPrdNombre(String prdNombre) {
        this.prdNombre = prdNombre;
    }

    public String getPrdDescripcion() {
        return prdDescripcion;
    }

    public void setPrdDescripcion(String prdDescripcion) {
        this.prdDescripcion = prdDescripcion;
    }

    public String getPrdEstado() {
        return prdEstado;
    }

    public void setPrdEstado(String prdEstado) {
        this.prdEstado = prdEstado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (prdCodigo != null ? prdCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Producto)) {
            return false;
        }
        Producto other = (Producto) object;
        return !((this.prdCodigo == null && other.prdCodigo != null) || (this.prdCodigo != null && !this.prdCodigo.equals(other.prdCodigo)));
    }

    public GrupoProducto getGrupoProducto() {
        return grupoProducto;
    }

    public void setGrupoProducto(GrupoProducto grupoProducto) {
        this.grupoProducto = grupoProducto;
    }

}
