package co.com.personalsoft.model;

import java.io.Serializable;

/**
 *
 * @author jmora
 */
public class GrupoProducto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer gprCodigo;

    private String gprNombre;

    private String grpEstado;

    public GrupoProducto() {
    }

    public GrupoProducto(Integer gprCodigo) {
        this.gprCodigo = gprCodigo;
    }

    public GrupoProducto(Integer gprCodigo, String gprNombre) {
        this.gprCodigo = gprCodigo;
        this.gprNombre = gprNombre;
    }

    public GrupoProducto(Integer gprCodigo, String gprNombre, String grpEstado) {
        this.gprCodigo = gprCodigo;
        this.gprNombre = gprNombre;
        this.grpEstado = grpEstado;
    }

    public Integer getGprCodigo() {
        return gprCodigo;
    }

    public void setGprCodigo(Integer gprCodigo) {
        this.gprCodigo = gprCodigo;
    }

    public String getGprNombre() {
        return gprNombre;
    }

    public void setGprNombre(String gprNombre) {
        this.gprNombre = gprNombre;
    }

    public String getGrpEstado() {
        return grpEstado;
    }

    public void setGrpEstado(String grpEstado) {
        this.grpEstado = grpEstado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gprCodigo != null ? gprCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof GrupoProducto)) {
            return false;
        }
        GrupoProducto other = (GrupoProducto) object;
        if ((this.gprCodigo == null && other.gprCodigo != null) || (this.gprCodigo != null && !this.gprCodigo.equals(other.gprCodigo))) {
            return false;
        }
        return true;
    }

}
