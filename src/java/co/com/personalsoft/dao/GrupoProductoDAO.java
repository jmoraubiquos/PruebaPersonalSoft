package co.com.personalsoft.dao;


import co.com.personalsoft.datasoruce.Conexion;
import co.com.personalsoft.model.GrupoProducto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

public class GrupoProductoDAO {

    public List<GrupoProducto> obtenerGrupoProductoActivo() throws SQLException {
        List<GrupoProducto> listaGrupos = new ArrayList<>();

        Conexion conex = new Conexion();
        try (Connection con = conex.establecerConexion();) {
            if (con != null) {
                PreparedStatement ps = con.prepareStatement("SELECT gpr_codigo, gpr_nombre, grp_estado FROM gruposproductos where grp_estado = 'A'");
                ResultSet rs = ps.executeQuery();

                while (rs.next()) {
                    listaGrupos.add(new GrupoProducto(rs.getInt(1), rs.getString(2), rs.getString(3)));
                }
            }
        }

        return listaGrupos;
    }
}
