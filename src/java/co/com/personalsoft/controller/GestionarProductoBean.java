package co.com.personalsoft.controller;

import co.com.personalsoft.dao.GrupoProductoDAO;
import co.com.personalsoft.dao.ProductoDAO;
import co.com.personalsoft.model.GrupoProducto;
import co.com.personalsoft.model.Producto;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author jmora
 */
@ManagedBean
@ViewScoped
public class GestionarProductoBean implements Serializable {

    private Producto productoSeleccionado;
    private List<Producto> listaProducto;

    private ProductoDAO productoDAO;
    private GrupoProductoDAO grupoProductoDAO;

    private List<GrupoProducto> listaGrupoProducto;

    @PostConstruct
    public void init() {
        try {
            productoSeleccionado = new Producto();
            productoDAO = new ProductoDAO();
            grupoProductoDAO = new GrupoProductoDAO();
            productoDAO = new ProductoDAO();
            setListaGrupoProducto(grupoProductoDAO.obtenerGrupoProductoActivo());
            listaProducto = productoDAO.obtenerProductos();
        } catch (SQLException ex) {
            Logger.getLogger(GestionarProductoBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void onEditar(Producto producto) {
        try {
            this.productoSeleccionado = producto;
        } catch (Exception ex) {
            Logger.getLogger(GestionarProductoBean.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    public void onEliminar(Producto producto) {
        try {
            productoDAO.eliminarProducto(producto);
            listaProducto=productoDAO.obtenerProductos();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Información", "Se ha eliminado correctamente el registro."));
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Exepción", "Se ha presentando una exepción, contacte con su Administrador"));
            Logger.getLogger(GestionarProductoBean.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    public void onAceptar(ActionEvent actionEvent) {
        try {

            if (productoSeleccionado.getPrdCodigo() != null) {
                productoDAO.actualizarProducto(productoSeleccionado);
            } else {

                productoDAO.registrarProducto(productoSeleccionado);
            }
            productoSeleccionado = new Producto();
            listaProducto = productoDAO.obtenerProductos();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Información", "Se ha guardado correctamente."));
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Exepción", "Se ha presentando una exepción, contacte con su Administrador"));
            Logger.getLogger(GestionarProductoBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * @return the productoSeleccionado
     */
    public Producto getProductoSeleccionado() {
        return productoSeleccionado;
    }

    /**
     * @param productoSeleccionado the productoSeleccionado to set
     */
    public void setProductoSeleccionado(Producto productoSeleccionado) {
        this.productoSeleccionado = productoSeleccionado;
    }

    /**
     * @return the listaProducto
     */
    public List<Producto> getListaProducto() {
        return listaProducto;
    }

    /**
     * @param listaProducto the listaProducto to set
     */
    public void setListaProducto(List<Producto> listaProducto) {
        this.listaProducto = listaProducto;
    }

    /**
     * @return the listaGrupoProducto
     */
    public List<GrupoProducto> getListaGrupoProducto() {
        return listaGrupoProducto;
    }

    /**
     * @param listaGrupoProducto the listaGrupoProducto to set
     */
    public void setListaGrupoProducto(List<GrupoProducto> listaGrupoProducto) {
        this.listaGrupoProducto = listaGrupoProducto;
    }
}
