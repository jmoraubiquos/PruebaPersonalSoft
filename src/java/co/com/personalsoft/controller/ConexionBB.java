package co.com.personalsoft.controller;

import co.com.personalsoft.datasoruce.Conexion;
import java.io.Serializable;

import javax.annotation.PostConstruct;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class ConexionBB implements Serializable {

    private Conexion conexionBd;
    private String conexionOk;

    public ConexionBB() {
        conexionBd = new Conexion();
    }

    @PostConstruct
    public void init() {
        conexionOk = conexionBd.verificarConexion();
    }

    public void setConexionOk(String conexionOk) {
        this.conexionOk = conexionOk;
    }

    public String getConexionOk() {
        return conexionOk;
    }
}
